'use strict';
var config = require('./config');

// по дополнительному параметру в getModel, getView, getController определяем с какого картриджа брать компоненты 
function getBase(inCurrentCartridge) {
	return inCurrentCartridge ? '~/' : config.get('controllerCartridge');
}

//перекрытие как в контроллере 
module.exports = require(config.getBasePath('controllerCartridge', 'cartridge/scripts/app'));

module.exports.getModel = function (modelName, inCurrentCartridge) {
	
	var base = getBase(inCurrentCartridge);
		
	return require(base + 'cartridge/scripts/models/' + modelName + 'Model');
};

module.exports.getView = function (viewName, parameters, inCurrentCartridge) {
	var base = getBase(inCurrentCartridge);
    var View;
    try {

        if (typeof viewName === 'string') {
            View = require(base + 'cartridge/scripts/views/' + viewName + 'View');
        } else {
            parameters = viewName;
            View = require(base + 'cartridge/scripts/views/View');
        }
    } catch (e) {
        View = require(base + 'cartridge/scripts/views/View');
    }
    return new View(parameters || {});
};

module.exports.getController = function (controllerName,inCurrentCartridge) {
	var base = getBase(inCurrentCartridge);
    return require(base + 'cartridge/controllers/' + controllerName);
};
