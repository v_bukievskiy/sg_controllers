'use strict';
var config = require('../config');
// импорт базового представления
var View = require(config.getBasePath('controllerCartridge', 'cartridge/scripts/views/View'));
// разширение представления
var NewsletterView = View.extend(
{
    template: 'newslettersignup',

    init: function (params) {
        this._super(params);
        this.ContinueURL = dw.web.URLUtils.https('Newsletter-SaveSubscription');
    }

});

module.exports = NewsletterView;
