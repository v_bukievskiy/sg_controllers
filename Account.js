'use strict'
//подключаем config, app с текущего картриджа и guard с базового
var cartridgeConf = require('~/cartridge/scripts/config'),
    app = require('~/cartridge/scripts/app'),
    guard = require(cartridgeConf.getBasePath('controllerCartridge', 'cartridge/scripts/guard'));
	
function editPreferences() {
    var preferences = app.getForm('preferences');
    preferences.copyFrom(customer.profile.custom, true);
    app.getView({
        ContinueURL: dw.web.URLUtils.https('Account-SavePreferences')
    }).render('editpreferences');
}

function savePreferences() {
    var preferencesForm = app.getForm('preferences');
    preferencesForm.handleAction({
        apply: function() {
            preferencesForm.copyTo(customer.profile.custom);
            response.redirect(dw.web.URLUtils.https('Account-Show'));
        },
        error: function() {
            app.getView({
                ContinueURL: dw.web.URLUtils.https('Account-SavePreferences')
            }).render
            return;
        }
    });
}

// импортируем контроллер с базового картриджа 
module.exports = require(cartridgeConf.getBasePath('controllerCartridge', 'cartridge/controllers/Account'));
//заменяем родительский action на свой
module.exports.EditPreferences = guard.ensure(['get'], editPreferences);
module.exports.SavePreferences = guard.ensure(['post'], savePreferences);
//Примичание: exports (без module) это хелпер, который записывает собраные свойства в module.exports, и если определен последний, он не работает 