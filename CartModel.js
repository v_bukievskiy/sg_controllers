'use strict'

var cartridgeConf = require('~/cartridge/scripts/config'),
    app = require('~/cartridge/scripts/app'),
    guard = require(cartridgeConf.getBasePath('controllerCartridge', 'cartridge/scripts/guard')),
    object = require(cartridgeConf.getBasePath('controllerCartridge', 'cartridge/scripts/object')),

    Pipelet = require('dw/system/Pipelet'),
    Transaction = require('dw/system/Transaction');

function addProductItem(product, quantity, cgid, productOptionModel) {
    var cart = this;
    Transaction.wrap(function() {
        var i;
        if (product) {

            var AddProductToBasketResult = new Pipelet('AddProductToBasket').execute({
                Basket: cart.object,
                Product: product,
                ProductOptionModel: productOptionModel,
                Quantity: quantity,
                Category: dw.catalog.CatalogMgr.getCategory(cgid)
            });

            if (AddProductToBasketResult.result === PIPELET_ERROR) {
                return;
            } else {
                var params = request.httpParameterMap,
                    productLineItem = AddProductToBasketResult.ProductLineItem;
                productLineItem.custom.pName = params.pName.stringValue;
                productLineItem.custom.pColor = params.pColor.stringValue;
                productLineItem.custom.pFont = params.pFont.stringValue;
            }

            if (product.bundle) {

                if (request.httpParameterMap.childPids.stringValue && product.bundle) {

                    if (request.httpParameterMap.childPids.stringValue) {
                        var childPids = request.httpParameterMap.childPids.stringValue.split(',');

                        for (i = 0; i < childPids.length; i++) {
                            var childProduct = Product.get(childPids[i]).object;

                            if (childProduct) {
                                // why is this needed ?
                                childProduct.updateOptionSelection(request.httpParameterMap);

                                var foundLineItem = null;
                                foundLineItem = this.getBundledProductLineItemByPID(lineItem, childProduct.isVariant() ? childProduct.masterProduct.ID : childProduct.ID);

                                if (foundLineItem) {
                                    foundLineItem.replaceProduct(childProduct);
                                }
                            }
                        }
                    }
                }
            }
            cart.calculate();
        }
    });
}
// импорт базового "класса"
var BaseModel = require(cartridgeConf.getBasePath('controllerCartridge', 'cartridge/scripts/models/CartModel'));
// "Наследуем класс" родительской модели
var CartModel = BaseModel.extend({
    addProductItem: addProductItem
});
// добавляем статические методы
CartModel.get = function(parameter) {
    var obj = null;

    if (!parameter) {
        var GetBasketResult = new Pipelet('GetBasket', {
            Create: false
        }).execute();
        if (GetBasketResult.result !== PIPELET_ERROR) {
            obj = GetBasketResult.Basket;
        }
    } else if (typeof parameter === 'object') {
        obj = parameter;
    }
    return (obj !== null) ? new CartModel(obj) : null;
};

CartModel.goc = function() {
    var obj = null;

    var GetBasketResult = new Pipelet('GetBasket', {
        Create: true
    }).execute();

    if (GetBasketResult.result !== PIPELET_ERROR) {
        obj = GetBasketResult.Basket;
    }

    return new CartModel(obj);
};

module.exports = CartModel;
