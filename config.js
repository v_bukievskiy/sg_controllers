'use strict';
var configHash = {
	coreCartridge  : "app_storefront_core/",
	controllerCartridge  : "app_storefront_controllers/"
}
exports.getBasePath = function (folder, path) {
	return	configHash[folder] + path;
};
exports.get = function(propName) {
	return (propName in configHash) ? configHash[propName] : undefined;
}